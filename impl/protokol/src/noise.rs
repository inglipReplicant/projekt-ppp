extern crate rand;

use std::fmt;

use noise::rand::distributions::Uniform;
use noise::rand::prelude::*;
use noise::rand::rngs::StdRng;
use shared::DATA_LENGTH;
use util::conversions::bytes_to_hex;

#[derive(Clone, Debug, PartialEq)]
pub struct IndexedSeed {
    pub u: usize,
    pub v: usize,
    pub seed: Vec<u8>,
}

impl fmt::Display for IndexedSeed {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "IndexedSeed ({} to {} : {})",
            self.u,
            self.v,
            bytes_to_hex(&self.seed)
        )
    }
}

pub fn filter_remaining(
    u2: &[usize],
    u3: &[usize],
    indexed_seeds: &[IndexedSeed],
) -> Vec<IndexedSeed> {
    let diff = u2
        .iter()
        .filter(|x| !u3.contains(&x))
        .cloned()
        .collect::<Vec<usize>>();
    indexed_seeds
        .iter()
        .filter(|s| diff.contains(&s.u) && u3.contains(&s.v))
        .cloned()
        .collect::<Vec<IndexedSeed>>()
}

pub fn gen_noise_u32(x: &[u32], own_seed: &[u8], indexed_seeds: &[IndexedSeed]) -> Vec<u32> {
    let mut w: [u32; DATA_LENGTH] = [0; DATA_LENGTH];
    add_inplace(&mut w, x);

    add_noise_inplace(&mut w, own_seed);
    for ind in indexed_seeds.iter() {
        let delta = ind.v as i32 - ind.u as i32;
        if delta > 0 {
            add_noise_inplace(&mut w, &ind.seed);
        } else if delta < 0 {
            sub_noise_inplace(&mut w, &ind.seed);
        }
    }
    Vec::from(&w[..])
}

pub fn gen_noise_f32(x: &[f32], own_seed: &[u8], indexed_seeds: &[IndexedSeed]) -> Vec<u32> {
    let mut w: [u32; DATA_LENGTH] = [0; DATA_LENGTH];
    f32_to_u32_inplace(x, &mut w);

    add_noise_inplace(&mut w, own_seed);

    for ind in indexed_seeds.iter() {
        let delta = ind.v as i32 - ind.u as i32;
        if delta > 0 {
            add_noise_inplace(&mut w, &ind.seed);
        } else if delta < 0 {
            sub_noise_inplace(&mut w, &ind.seed);
        }
    }
    Vec::from(&w[..])
}

pub fn undo_noise_u32(
    ys: &[Vec<u32>],
    survivor_seeds: &[Vec<u8>],
    filtered_dropped_seeds: &[IndexedSeed],
) -> Vec<u32> {
    let mut w: [u32; DATA_LENGTH] = [0; DATA_LENGTH];

    println!("Started reconstruction.");
    for y in ys {
        add_inplace(&mut w, y);
    }

    println!();
    for s in survivor_seeds.iter() {
        sub_noise_inplace(&mut w, &s[..]);
    }

    for s in filtered_dropped_seeds.iter() {
        let delta = s.v as i32 - s.u as i32;
        if delta > 0 {
            add_noise_inplace(&mut w, &s.seed);
        } else if delta < 0 {
            sub_noise_inplace(&mut w, &s.seed);
        }
    }
    Vec::from(&w[..])
}

pub fn undo_noise_f32(
    ys: &[Vec<u32>],
    survivor_seeds: &[Vec<u8>],
    filtered_dropped_seeds: &[IndexedSeed],
) -> Vec<f32> {
    let mut w: [u32; DATA_LENGTH] = [0; DATA_LENGTH];

    println!("Started reconstruction.");
    for y in ys {
        add_inplace(&mut w, y);
    }

    println!();
    for s in survivor_seeds.iter() {
        sub_noise_inplace(&mut w, &s[..]);
    }

    for s in filtered_dropped_seeds.iter() {
        let delta = s.v as i32 - s.u as i32;
        if delta > 0 {
            add_noise_inplace(&mut w, &s.seed);
        } else if delta < 0 {
            sub_noise_inplace(&mut w, &s.seed);
        }
    }
    let mut x: [f32; DATA_LENGTH] = [0.0; DATA_LENGTH];
    u32_to_f32_inplace(&w, &mut x);
    Vec::from(&x[..])
}

pub fn undo_noise_f32_mod(
    ys: &[Vec<u32>],
    survivor_seeds: &[Vec<u8>],
    filtered_dropped_seeds: &[IndexedSeed],
) -> Vec<f32> {
    let res = undo_noise_u32(ys, survivor_seeds, filtered_dropped_seeds);
    let mut temp = [0.0; DATA_LENGTH];
    u32_to_f32_inplace(&res[..], &mut temp[..]);
    Vec::from(&temp[..])
}

fn add_inplace(dest: &mut [u32], src: &[u32]) {
    for (d, s) in dest.iter_mut().zip(src.iter()) {
        *d = d.wrapping_add(*s);
    }
}

#[allow(dead_code)]
fn sub_inplace(dest: &mut [u32], src: &[u32]) {
    for (d, s) in dest.iter_mut().zip(src.iter()) {
        *d = d.wrapping_sub(*s);
    }
}

fn add_noise_inplace(w: &mut [u32], seed: &[u8]) {
    let mut local_seed = [0u8; 32];
    let between = Uniform::from(0..u32::max_value());

    for (local, outer) in local_seed.iter_mut().zip(seed.into_iter()) {
        *local = *outer;
    }
    let mut rng = StdRng::from_seed(local_seed);

    for val in w.iter_mut() {
        *val = val.wrapping_add(between.sample(&mut rng));
    }
}

fn sub_noise_inplace(w: &mut [u32], seed: &[u8]) {
    let mut local_seed = [0u8; 32];
    let between = Uniform::from(0..u32::max_value());

    for (local, outer) in local_seed.iter_mut().zip(seed.into_iter()) {
        *local = *outer;
    }
    let mut rng = StdRng::from_seed(local_seed);

    for val in w.iter_mut() {
        *val = val.wrapping_sub(between.sample(&mut rng));
    }
}

fn f32_to_u32_inplace(x: &[f32], w: &mut [u32]) {
    for (float, int) in x.into_iter().zip(w.iter_mut()) {
        *int = float.to_bits();
    }
}

fn u32_to_f32_inplace(w: &[u32], x: &mut [f32]) {
    for (int, float) in w.into_iter().zip(x.iter_mut()) {
        *float = f32::from_bits(*int);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_sub_noise() {
        let mut dest = [0; 10];
        let x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

        add_inplace(&mut dest, &x);
        assert_eq!(dest, x);

        sub_inplace(&mut dest, &x);
        assert_eq!(dest, [0; 10]);
    }

    #[test]
    fn test_add_sub_seeded_noise() {
        let seed = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let init_values = [22, 33, 44, 55, 66];
        let mut values = [22, 33, 44, 55, 66];

        add_noise_inplace(&mut values, &seed);
        assert_ne!(init_values, values);

        sub_noise_inplace(&mut values, &seed);
        assert_eq!(init_values, values);
    }

    #[test]
    fn test_conversion() {
        let x = [1.1, 2.2, 3.3, 4.4, 5.5];
        let mut w = [0 as u32; 5];
        f32_to_u32_inplace(&x, &mut w);

        let mut new_x = [0 as f32; 5];
        u32_to_f32_inplace(&w, &mut new_x);
        assert_eq!(x, new_x);
    }

    #[test]
    fn test_index_filtering() {
        let u2 = [1, 2, 3];
        let u3 = [1, 3];

        let diff = u2
            .iter()
            .filter(|x| !u3.contains(&x))
            .cloned()
            .collect::<Vec<usize>>();
        assert_eq!(diff, [2]);
    }
}

pub mod test_util {
    use super::*;

    pub fn create_zeroes() -> Vec<Vec<f32>> {
        vec![
            vec![0.0, 0.0, 0.0],
            vec![0.0, 0.0, 0.0],
            vec![0.0, 0.0, 0.0],
        ]
    }

    pub fn create_xs() -> Vec<Vec<u32>> {
        let x1 = vec![1, 2, 3];
        let x2 = vec![4, 5, 6];
        let x3 = vec![7, 8, 9];

        let mut res = Vec::new();
        res.push(x1);
        res.push(x2);
        res.push(x3);
        res
    }

    pub fn create_xs_f32() -> Vec<Vec<f32>> {
        let x1 = vec![1.0, 2.0, 3.0];
        let x2 = vec![4.0, 5.0, 6.0];
        let x3 = vec![7.0, 8.0, 9.0];

        let mut res = Vec::new();
        res.push(x1);
        res.push(x2);
        res.push(x3);
        res
    }

    pub fn create_seeds() -> Vec<Vec<u8>> {
        let mut res = Vec::new();
        res.push(vec![
            11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
        ]);
        res.push(vec![
            22, 33, 44, 55, 66, 77, 88, 99, 11, 22, 33, 44, 55, 66, 77, 88,
        ]);
        res.push(vec![
            13, 24, 42, 52, 32, 43, 76, 78, 54, 75, 75, 43, 21, 53, 12, 19,
        ]);
        res
    }

    pub fn create_indexed() -> Vec<IndexedSeed> {
        let s12 = IndexedSeed {
            u: 1,
            v: 2,
            seed: vec![1, 2, 3, 4, 5, 6, 7, 8],
        };

        let s21 = IndexedSeed {
            u: 2,
            v: 1,
            seed: vec![1, 2, 3, 4, 5, 6, 7, 8],
        };

        let s13 = IndexedSeed {
            u: 1,
            v: 3,
            seed: vec![11, 22, 33, 44, 55, 66, 77, 88],
        };

        let s31 = IndexedSeed {
            u: 3,
            v: 1,
            seed: vec![11, 22, 33, 44, 55, 66, 77, 88],
        };

        let s23 = IndexedSeed {
            u: 2,
            v: 3,
            seed: vec![0, 21, 32, 43, 54, 65, 76, 87, 97, 98, 12, 4, 2],
        };

        let s32 = IndexedSeed {
            u: 3,
            v: 2,
            seed: vec![0, 21, 32, 43, 54, 65, 76, 87, 97, 98, 12, 4, 2],
        };

        vec![s12, s21, s13, s31, s23, s32]
    }

    pub fn calculate_ys(xs: &[Vec<u32>], ps: &[Vec<u8>], indexed: &[IndexedSeed]) -> Vec<Vec<u32>> {
        let mut ys = Vec::new();

        let y1 = gen_noise_u32(
            xs.get(0).unwrap(),
            ps.get(0).unwrap(),
            &[
                indexed.get(0).unwrap().clone(),
                indexed.get(2).unwrap().clone(),
            ],
        );
        ys.push(y1);

        let y2 = gen_noise_u32(
            xs.get(1).unwrap(),
            ps.get(1).unwrap(),
            &[
                indexed.get(1).unwrap().clone(),
                indexed.get(4).unwrap().clone(),
            ],
        );
        ys.push(y2);

        let y3 = gen_noise_u32(
            xs.get(2).unwrap(),
            ps.get(2).unwrap(),
            &[
                indexed.get(3).unwrap().clone(),
                indexed.get(5).unwrap().clone(),
            ],
        );
        ys.push(y3);
        ys
    }

    pub fn calculate_ys_f32(
        xs: &[Vec<f32>],
        ps: &[Vec<u8>],
        indexed: &[IndexedSeed],
    ) -> Vec<Vec<u32>> {
        let mut ys = Vec::new();

        let y1 = gen_noise_f32(
            xs.get(0).unwrap(),
            ps.get(0).unwrap(),
            &[
                indexed.get(0).unwrap().clone(),
                indexed.get(2).unwrap().clone(),
            ],
        );
        ys.push(y1);

        let y2 = gen_noise_f32(
            xs.get(1).unwrap(),
            ps.get(1).unwrap(),
            &[
                indexed.get(1).unwrap().clone(),
                indexed.get(4).unwrap().clone(),
            ],
        );
        ys.push(y2);

        let y3 = gen_noise_f32(
            xs.get(2).unwrap(),
            ps.get(2).unwrap(),
            &[
                indexed.get(3).unwrap().clone(),
                indexed.get(5).unwrap().clone(),
            ],
        );
        ys.push(y3);
        ys
    }
}
