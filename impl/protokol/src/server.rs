//! # A simple HTTP server
//!

#![allow(deprecated)]
extern crate bincode;
extern crate chrono;
extern crate linalg;
extern crate mio;
extern crate ml;
extern crate openssl;
extern crate serde;
extern crate slab;
extern crate sodiumoxide;
extern crate timer;

use std::collections::{HashMap, VecDeque};
use std::io::{Read, Result, Write};
use std::sync::{Arc, Mutex};
use std::time::Duration;

#[allow(unused_imports)]
use server::bincode::{deserialize, serialize};
use server::chrono as chrono_time;
use server::mio::net::TcpListener;
use server::mio::{Events, Poll, PollOpt, Ready, Token};
use server::openssl::bn::BigNum;
use server::sodiumoxide::crypto::box_::curve25519xsalsa20poly1305 as asym_ae;
use server::timer::Timer;

use noise_f::{noise_undo, IndexedSeed};
use serdata::unmasking::UnmaskingShareVec;
use serdata::*;
use shared::*;
use sss::{ShamirScheme, Share};
use util::conversions;

use self::linalg::vector::{RealVec, Vector};
use self::ml::classif::logreg::LogReg2D;

type Slab<T> = slab::Slab<T>;

type IndexedKey = (usize, Vec<u8>);

pub struct Connections {
    pub serv_token: Token,

    pub events: Events,

    pub poll: Poll,

    pub peers: Slab<Peer>,

    pub listener: TcpListener,
}

impl Connections {
    pub fn from_ip(ip: &str) -> Result<Connections> {
        let conns = Connections {
            serv_token: Token(SERVER_TOKEN),
            events: Events::with_capacity(POLL_LIMIT * 10),
            poll: Poll::new()?,
            peers: Slab::with_capacity(POLL_LIMIT),
            listener: TcpListener::bind(&ip.parse().unwrap())?,
        };
        Ok(conns)
    }

    pub fn init(&mut self) -> Result<()> {
        self.poll.register(
            &self.listener,
            self.serv_token,
            Ready::readable(),
            PollOpt::edge(),
        )?;
        Ok(())
    }
}

pub struct Server {
    pub pub_k: asym_ae::PublicKey,

    sec_k: asym_ae::SecretKey,

    peer_data: HashMap<usize, ClientData>,

    encrypted_share_vec: Option<sharing::EncryptedShareVec>,

    ys: Vec<Vec<BigNum>>,

    u2: Vec<usize>,

    u3: Vec<usize>,

    _u5: Vec<usize>,

    dropout_s_keys: Vec<IndexedKey>,

    survivor_seeds: Vec<Vec<u8>>,

    dropout_key_map: HashMap<usize, (usize, Vec<Share>)>,

    survivor_seed_map: HashMap<usize, Vec<Share>>,

    dropout_seeds: Vec<IndexedSeed>,

    ml_weights: RealVec,
}

impl Server {
    pub fn new(pub_key: asym_ae::PublicKey, sec_key: asym_ae::SecretKey) -> Result<Self> {
        let server = Server {
            pub_k: pub_key,
            sec_k: sec_key,
            peer_data: HashMap::new(),
            encrypted_share_vec: None,
            ys: Vec::new(),
            u2: Vec::new(),
            u3: Vec::new(),
            _u5: Vec::new(),
            dropout_s_keys: Vec::new(),
            survivor_seeds: Vec::new(),
            dropout_key_map: HashMap::new(),
            survivor_seed_map: HashMap::new(),
            dropout_seeds: Vec::new(),
            ml_weights: RealVec::from_vec(vec![0.0; WEIGHT_LEN]),
        };
        Ok(server)
    }

    fn collect_clients(&mut self, conns: &mut Connections) -> Result<()> {
        let mut buf = [0u8; 32];

        loop {
            conns
                .poll
                .poll(&mut conns.events, Some(Duration::from_millis(100)))?;

            for event in conns.events.iter() {
                let (token, kind) = (event.token(), event.readiness());
                //println!("Token {} of kind {:?}", token.0, kind);

                if kind.is_error() || kind.is_hup() {
                    if token.0 == SERVER_TOKEN {
                        panic!("Internal server error.");
                    }
                    conns.peers.remove(token.0);
                    self.peer_data.remove(&token.0);
                    println!("Client {:?} dropped connection", token);
                    continue;
                }

                if kind.is_readable() {
                    //println!("Readable");
                    match token.0 {
                        //server socket has been polled and is ready, so accept the new conn
                        SERVER_TOKEN => {
                            let (sock, _addr) = match conns.listener.accept() {
                                Ok((sock, addr)) => (sock, addr),
                                Err(_) => panic!("Error while accepting the request."),
                            };
                            //println!("Accepted!");
                            let entry = conns.peers.vacant_entry();
                            let c = Peer {
                                socket: sock,
                                write_queue: VecDeque::new(),
                                agreed: false,
                                token_index: entry.key(),
                                peer_precomputed_key: None,
                                nonce: Some(asym_ae::gen_nonce()),
                                peer_data: ClientData::new(token.0),
                            };
                            conns.poll.register(
                                &c.socket,
                                Token(entry.key()),
                                Ready::all(),
                                PollOpt::edge(),
                            )?;
                            entry.insert(c);
                        }
                        _ => {
                            if conns.peers.len() > NUM_OF_USRS {
                                println!("Max num of users reached ({}).", NUM_OF_USRS);
                                let ref mut peer = conns.peers[token.0];
                                let _ = peer.socket.read(&mut buf);
                                return Ok(());
                            }
                            {
                                let ref mut peer = conns.peers[token.0];
                                let _bytes = match peer.socket.read(&mut buf) {
                                    Ok(bytes) => bytes,
                                    Err(_) => break,
                                };

                                match peer.peer_precomputed_key {
                                    Some(_) => continue,
                                    None => {
                                        let client_pub_key =
                                            match asym_ae::PublicKey::from_slice(&buf) {
                                                Some(key) => key,
                                                _ => panic!(
                                                "Unable to reconstruct the client's key. Aborting."
                                            ),
                                            };
                                        let key = asym_ae::precompute(&client_pub_key, &self.sec_k);
                                        peer.peer_precomputed_key = Some(key);
                                    }
                                }
                            }
                        }
                    }
                }
                if kind.is_writable() {
                    //println!("Writable");
                    let ref mut peer = conns.peers[token.0];

                    if peer.agreed {
                        break;
                    }
                    let bytes = match peer.socket.write(&self.pub_k.0) {
                        Ok(b) => b,
                        Err(_) => break,
                    };

                    let _ = match peer.socket.write(&peer.nonce.unwrap().0) {
                        Ok(b) => bytes + b,
                        Err(_) => break,
                    };
                    //println!("Sent {}B", bytes);
                    peer.agreed = true;
                }
            }
            if conns.peers.len() == NUM_OF_USRS && !conns.peers.iter().any(|(_, c)| !c.agreed) {
                return Ok(());
            }
        }
    }

    fn flush_invalid(&mut self, conns: &mut Connections) {
        let len = conns.peers.len();
        for i in 0..len {
            let nonce = conns.peers[i].nonce;
            if nonce.is_none() {
                conns.peers.remove(i);
            }
        }
        for i in 0..len {
            let mut remove = false;
            {
                let key = &mut conns.peers[i].peer_precomputed_key;
                if key.is_none() {
                    remove = true;
                }
            }
            if remove {
                conns.peers.remove(i);
            }
        }

        for (c, _) in conns.peers.iter() {
            conns
                .poll
                .reregister(
                    &conns.listener,
                    Token(c),
                    Ready::readable() | Ready::writable(),
                    PollOpt::edge() | PollOpt::oneshot(),
                )
                .unwrap();
        }
    }

    fn run_round<F>(
        &mut self,
        func: F,
        conns: &mut Connections,
        round_name: &str,
        op: Ready,
    ) -> Result<()>
    where
        F: Fn(&mut Server, &mut Peer) -> Result<()>,
    {
        //Timer-related initializations
        let timer = Timer::new();
        let done = Arc::new(Mutex::new(false));
        let flag = done.clone();
        let time = 15; //time in seconds
        let init_count = conns.peers.len();
        let mut handled_clients = 0;
        let _guard = timer.schedule_with_delay(chrono_time::Duration::seconds(time), move || {
            let mut is_done = flag.lock().unwrap();
            *is_done = true;
        });
        //TODO: Remove test print
        println!(
            "!!! STARTING ROUND {} WITH {} USERS !!!",
            round_name,
            conns.peers.len()
        );
        loop {
            {
                let done = done.lock().unwrap();
                if *done || handled_clients == init_count {
                    println!("Round complete!");
                    return Ok(());
                }
            }
            conns.poll.poll(&mut conns.events, None)?;

            for event in conns.events.iter() {
                let (token, kind) = (event.token(), event.readiness());
                //println!("Token {} of kind {:?}", token.0, kind);

                if !conns.peers.contains(token.0) {
                    //println!("Skipping {}", token.0);
                    continue;
                }

                if kind.is_error() || kind.is_hup() {
                    if token.0 == SERVER_TOKEN {
                        panic!("Internal server error.");
                    }
                    conns.peers.remove(token.0);
                    self.peer_data.remove(&token.0);
                    println!("Client {:?} dropped connection", token);
                } else if kind.contains(op) {
                    //println!("Handling peer {}", token.0);
                    let res = func(self, &mut conns.peers[token.0]);
                    match res {
                        Ok(_) => {
                            handled_clients += 1;
                            continue;
                        }
                        Err(e) => panic!(
                            "Error while handling the client in round {} for token {}: {}",
                            round_name, token.0, e
                        ),
                    };
                }
            }
        }
    }

    fn reconstruct_public_keys(&mut self, peer: &mut Peer) -> Result<()> {
        let plaintext = self.read_encrypted(peer)?;
        let c_pk = asym_ae::PublicKey::from_slice(&plaintext[0..32])
            .expect("Unable to reconstruct c_pk from received bytes");
        let s_pk = asym_ae::PublicKey::from_slice(&plaintext[32..64])
            .expect("Unable to reconstruct s_pk from received bytes");
        let ref mut data = peer.peer_data;

        data.c_pub_key = Some(c_pk);
        data.s_pub_key = Some(s_pk);

        let c_pk = asym_ae::PublicKey::from_slice(&plaintext[0..32])
            .expect("Unable to reconstruct c_pk from received bytes");
        let s_pk = asym_ae::PublicKey::from_slice(&plaintext[32..64])
            .expect("Unable to reconstruct s_pk from received bytes");

        let client_data = ClientData {
            token_index: peer.token_index,
            c_pub_key: Some(c_pk),
            c_sec_key: None,
            s_pub_key: Some(s_pk),
            s_sec_key: None,
        };
        self.peer_data.insert(peer.token_index, client_data);
        Ok(())
    }

    fn send_indexed_pubkeys(&mut self, peer: &mut Peer) -> Result<()> {
        let client_data: Vec<ClientData> = self.peer_data.values().map(|x| x.clone()).collect();
        let ref plaintext = advertisement::serialize_advertisement_round(
            &client_data[..],
            self.ml_weights.values(),
        );
        let _ = match self.send_encrypted(&plaintext, peer) {
            Ok(b) => b,
            Err(e) => panic!(e),
        };
        //println!("Sent {}B of data to peer {} in the advertisement round", bytes, peer.token_index);
        Ok(())
    }

    //todo: return return value to Result<EncryptedShareVec>
    fn collect_enc_shares_single(&mut self, peer: &mut Peer) -> Result<()> {
        let encoded = self.read_encrypted(peer)?;
        //let _ = encoded.len();
        let decoded: sharing::EncryptedShareVec = deserialize(&encoded)
            .expect("Unable to deserialize the encoded secret share vector. Aborting.");

        //println!("Read {}B encoded from {}", len, peer.token_index);
        self.u2.push(peer.token_index);

        if self.encrypted_share_vec.is_none() {
            self.encrypted_share_vec = Some(decoded);
        } else {
            let ref mut vec = self.encrypted_share_vec.as_mut().unwrap();
            for m in decoded.message.iter().cloned() {
                vec.message.push(m);
            }
        }
        Ok(())
    }

    fn send_encrypted_share_vec(&mut self, peer: &mut Peer) -> Result<()> {
        let encoded = serialize(&self.encrypted_share_vec.as_ref().unwrap())
            .expect("Unable to serialize the encrypted share vector when sending to peer");

        let _ = match self.send_encrypted(&encoded, peer) {
            Ok(b) => b,
            Err(e) => panic!("Couldnt send to peer {}: {}", peer.token_index, e),
        };
        //println!("Successfully sent the vector of shares to peer {}: {}B",
        //    peer.token_index, bytes);
        Ok(())
    }

    fn collect_masked_vectors(&mut self, peer: &mut Peer) -> Result<()> {
        let encoded = self.read_encrypted(peer)?;
        let decoded: Vec<Vec<u8>> =
            deserialize(&encoded).expect("Unable to deserialize masked vector");
        let decoded: Vec<BigNum> = decoded
            .iter()
            .map(|v| BigNum::from_slice(&v[..]).unwrap())
            .collect::<Vec<BigNum>>();
        //println!("Read ys: {:?}", decoded);
        self.ys.push(decoded);
        self.u3.push(peer.token_index);
        Ok(())
    }

    fn send_users_u3(&mut self, peer: &mut Peer) -> Result<()> {
        let encoded = serialize(&self.u3).expect("Unable to serialize u3");
        let _ = self.send_encrypted(&encoded, peer)?;
        //println!("Sent {}B to {}", bytes, peer.token_index);
        Ok(())
    }

    fn collect_decrypted_share_vec(&mut self, peer: &mut Peer) -> Result<()> {
        let encoded = self.read_encrypted(peer)?;
        let decoded: UnmaskingShareVec =
            deserialize(&encoded).expect("Unable to deserialize decrypted share vector");
        let ref mut dropout_key_map = self.dropout_key_map;
        let ref mut survivor_seed_map = self.survivor_seed_map;

        for share in decoded.shares.iter() {
            let ind = share.owner_index;
            if share.is_key_share {
                //println!("Inserting to dropouts");
                let (_, share_vec) = dropout_key_map
                    .entry(ind)
                    .or_insert((share.owner_index, Vec::new()));
                share_vec.push(share.share.to_share()?);
            } else {
                //println!("Inserting to survivors");
                let share_vec = survivor_seed_map.entry(ind).or_insert(Vec::new());
                share_vec.push(share.share.to_share()?);
            }
        }
        Ok(())
    }

    fn calculate_sum(&mut self) -> Result<Vec<f64>> {
        self.reconstruct_shares()
            .expect("Unable to reconstruc the shares.");
        self.calculate_indexed_seeds()
            .expect("Unable to calculate the sum.");
        Ok(self.reconstruct_ys())
    }

    fn reconstruct_ys(&mut self) -> Vec<f64> {
        noise_undo(
            &self.ys[..],
            &self.survivor_seeds[..],
            &self.dropout_seeds[..],
        )
        .expect("Unable to reconstruct ys")
    }

    fn reconstruct_shares(&mut self) -> Result<()> {
        let prime = BigNum::from_dec_str(MAX_SECRET_DEC_STR)?;
        let scheme = ShamirScheme::from_bn(&prime, NUM_OF_USRS, SS_THRESHOLD)?;
        for (v, shares) in self.dropout_key_map.values() {
            let secret_key = scheme.recover_secret(shares)?;
            //println!("Reconstructed secret key: {}", conversions::bytes_to_hex(&secret_key.to_vec()));
            self.dropout_s_keys.push((*v, secret_key.to_vec()));
        }

        for shares in self.survivor_seed_map.values() {
            let seed = scheme.recover_secret(shares)?;
            //println!("Reconstructed seed: {}", conversions::bytes_to_hex(&seed.to_vec()));
            self.survivor_seeds.push(seed.to_vec());
        }
        Ok(())
    }

    fn calculate_indexed_seeds(&mut self) -> Result<()> {
        let ref mut seeds: &mut Vec<IndexedSeed> = self.dropout_seeds.as_mut();
        let ref keys: &Vec<IndexedKey> = self.dropout_s_keys.as_ref();
        let u2 = self.u2.clone();
        let diff = self
            .u3
            .iter()
            .filter(|s| !u2.contains(*s))
            .cloned()
            .collect::<Vec<usize>>();

        println!("Calculating for {} users", u2.len());
        for (u, key) in keys.iter() {
            for v in diff.iter() {
                let peer_data = self.peer_data.get(&v).expect("No peer data found");
                let v_pub_key = peer_data.s_pub_key.expect("No pub key");
                let sec_key = asym_ae::SecretKey::from_slice(key)
                    .expect("Unable to create the secure key from byte slice.");
                let precomputed = asym_ae::precompute(&v_pub_key, &sec_key);
                let ind = IndexedSeed {
                    u: *u,
                    v: *v,
                    seed: Vec::from(&precomputed.0[..]),
                };
                seeds.push(ind);
            }
        }
        println!("Crafted keypairs");
        Ok(())
    }

    fn read_encrypted(&mut self, peer: &mut Peer) -> Result<Vec<u8>> {
        let mut buf = [0u8; BUFFER_SIZE];
        let ref mut nonce = peer.nonce.as_mut().unwrap();
        let mut ciphertext = Vec::new();

        let key = match peer.peer_precomputed_key {
            Some(ref key) => key,
            _ => panic!("No precomputed key provided for peer {}.", peer.token_index),
        };
        loop {
            // let mut peek_buf = [0; 1];
            // let _ = match peer.socket.peek(&mut peek_buf) {
            //     Ok(a) => a,
            //     _ => break
            // };
            let bytes = peer.socket.read(&mut buf)?;
            //println!("Read {} bytes encrypted", bytes);
            //println!("Decrypting with nonce {}", conversions::byte_slice_to_hex(&nonce.0));
            ciphertext.append(&mut buf[0..bytes].into_iter().map(|x| *x).collect::<Vec<u8>>());
            if bytes < BUFFER_SIZE {
                break;
            }
        }
        //println!("Decrypting with nonce {}", conversions::byte_slice_to_hex(&nonce.0));
        let plaintext = match asym_ae::open_precomputed(&ciphertext, &nonce, key) {
            Ok(m) => m,
            _ => panic!(
                "Unable to decrypt and/or verify the contents of the message received by peer {} decrypted by key {}",
                    peer.token_index, conversions::byte_slice_to_hex(&key.0))
        };
        nonce.increment_le_inplace();
        Ok(plaintext)
    }

    fn send_encrypted(&mut self, plaintext: &[u8], peer: &mut Peer) -> Result<usize> {
        let ref mut nonce = peer.nonce.as_mut().unwrap();
        let key = match &peer.peer_precomputed_key {
            Some(k) => k,
            None => panic!("No precomputed key found towards peer. Aborting."),
        };
        //println!("Encrypting with nonce {}", conversions::byte_slice_to_hex(&nonce.0));
        let ciphertext = asym_ae::seal_precomputed(&plaintext, &nonce, &key);
        let bytes = peer
            .socket
            .write(&ciphertext)
            .expect("Unable to send all of the data.");
        nonce.increment_le_inplace();
        Ok(bytes)
    }

    pub fn initialize(&mut self, conns: &mut Connections) -> Result<()> {
        self.collect_clients(conns)?;
        self.flush_invalid(conns);
        collect_public_keys(self, conns)?;
        self.flush_invalid(conns);
        Ok(())
    }

    fn cleanup(&mut self) {
        self.encrypted_share_vec = None;
        self.ys = Vec::new();
        self.u2 = Vec::new();
        self.u3 = Vec::new();
        self._u5 = Vec::new();
        self.dropout_s_keys = Vec::new();
        self.survivor_seeds = Vec::new();
        self.dropout_key_map = HashMap::new();
        self.survivor_seed_map = HashMap::new();
        self.dropout_seeds = Vec::new();
        self.encrypted_share_vec = None;
    }

    pub fn run_protocol_iter(&mut self, conns: &mut Connections) -> Result<Vec<f64>> {
        self.cleanup();
        broadcast_pubkeys_2(self, conns)?;
        self.flush_invalid(conns);
        collect_encrypted_shares(self, conns)?;
        distribute_encrypted_shares(self, conns)?;
        collect_masked_vectors(self, conns)?;
        distribute_surviving_users(self, conns)?;
        collect_decrypted_shares(self, conns)?;
        self.calculate_sum()
    }

    pub fn update_weights(&mut self, grad_vec: Vec<f64>) -> Result<()> {
        let learning_rate = 0.1;
        let gradient = RealVec::from_vec(grad_vec);
        let mut gradient = gradient
            .from_homogeneous()
            .expect("Math error when converting from homogenous.");
        println!("Gradient: {}", gradient);
        gradient.scale_inplace(learning_rate);
        println!("Gradient after scaling: {}", gradient);
        self.ml_weights.sub_inplace(&gradient).expect("Math error.");
        Ok(())
    }

    pub fn get_weights(&self) -> &RealVec {
        &self.ml_weights
    }

    pub fn assess_model(&self) {
        println!("Assessing model...");
        let logreg = LogReg2D::from_weights_simulation(
            self.ml_weights.clone(),
            Some(&vec![1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
        );
        let acc = logreg.validate();
        println!("Model accuracy is: {}", acc);
    }
}

fn collect_public_keys(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "Advertise keys: collect public keys";
    let kind = Ready::readable();
    serv.run_round(
        |serv, ref mut peer| serv.reconstruct_public_keys(peer),
        conns,
        &round_name,
        kind,
    )?;
    Ok(())
}

pub fn broadcast_pubkeys(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "Advertise keys: distribute indexed public keys";
    let kind = Ready::readable();
    serv.run_round(
        |serv, ref mut peer| serv.send_indexed_pubkeys(peer),
        conns,
        &round_name,
        kind,
    )?;
    Ok(())
}

fn broadcast_pubkeys_2(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    for (_, peer) in conns.peers.iter_mut() {
        serv.send_indexed_pubkeys(peer)?;
    }
    Ok(())
}

fn collect_encrypted_shares(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "ShareKeys: collect encrypted secret shares";
    let kind = Ready::readable();
    serv.run_round(
        |serv, ref mut peer| serv.collect_enc_shares_single(peer),
        conns,
        &round_name,
        kind,
    )?;
    Ok(())
}

fn distribute_encrypted_shares(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "ShareKeys: distribute encrypted secret shares";
    println!("{}", round_name);
    //let kind = Ready::writable();
    for (_, peer) in conns.peers.iter_mut() {
        serv.send_encrypted_share_vec(peer)?;
    }
    Ok(())
}

fn collect_masked_vectors(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "MaskedInputCollection: collect masked inputs";
    let kind = Ready::readable();
    serv.run_round(
        |serv, ref mut peer| serv.collect_masked_vectors(peer),
        conns,
        &round_name,
        kind,
    )?;
    Ok(())
}

fn distribute_surviving_users(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "MaskedInputCollection: distribute set of remaining users U3";
    println!("{}", round_name);

    for (_, peer) in conns.peers.iter_mut() {
        serv.send_users_u3(peer)?
    }
    Ok(())
}

fn collect_decrypted_shares(serv: &mut Server, conns: &mut Connections) -> Result<()> {
    let round_name = "Unmasking: collect decrypted secret shares";
    let kind = Ready::readable();
    serv.run_round(
        |serv, ref mut peer| serv.collect_decrypted_share_vec(peer),
        conns,
        &round_name,
        kind,
    )?;
    Ok(())
}
