extern crate bincode;
extern crate sodiumoxide;

use std::fmt;

#[allow(unused_imports)]
use self::bincode::{deserialize, serialize};
use self::sodiumoxide::crypto::box_;
use self::sodiumoxide::crypto::box_::curve25519xsalsa20poly1305 as asym_ae;

use shared::*;
use sss::ser_shares::SerializableShare;
use util::conversions;

pub mod advertisement {
    use super::*;

    #[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
    pub struct DataAdvertise {
        pub v: usize,

        pub c_pk: box_::PublicKey,

        pub s_pk: box_::PublicKey,

        pub ws: Vec<f64>,
    }

    impl fmt::Display for DataAdvertise {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(
                f,
                "Client: ({}, {}, {})",
                self.v,
                conversions::byte_slice_to_hex(&self.c_pk.0),
                conversions::byte_slice_to_hex(&self.s_pk.0)
            )
        }
    }

    pub fn serialize_advertisement_round(
        client_data: &[ClientData],
        weights: &Vec<f64>,
    ) -> Vec<u8> {
        let mut data = Vec::new();
        for client in client_data.iter() {
            let ref cpk = match client.c_pub_key {
                Some(ref v) => v,
                None => panic!("No public key to send to clients in the advertisement round."),
            };
            let ref spk = match client.s_pub_key {
                Some(ref v) => v,
                None => panic!("No public key to send to clients in the advertisement round."),
            };
            let temp_data = DataAdvertise {
                v: client.token_index,
                c_pk: asym_ae::PublicKey::from_slice(&cpk.0).unwrap(),
                s_pk: asym_ae::PublicKey::from_slice(&spk.0).unwrap(),
                ws: weights.iter().cloned().collect::<Vec<f64>>(),
            };
            data.push(temp_data);
        }
        let encoded: Vec<u8> = match serialize(&data) {
            Ok(d) => d,
            Err(e) => panic!(
                "Unable to serialize data in the advertisement round. Full error: {}",
                e
            ),
        };
        encoded
    }
}

pub mod sharing {
    use super::*;

    #[derive(Serialize, Deserialize, PartialEq)]
    pub struct DataShares {
        pub user_index: usize,

        pub peer_index: usize,

        pub key_share: SerializableShare,

        pub seed_share: SerializableShare,
    }

    impl fmt::Debug for DataShares {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(
                f,
                "From {} to {}: KS: {}, SS: {}",
                self.user_index, self.peer_index, self.key_share, self.seed_share
            )
        }
    }

    #[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
    pub struct EncryptedSerShare {
        pub u: usize,

        pub v: usize,

        pub nonce: Vec<u8>,

        pub ciphertext: Vec<u8>,
    }

    impl EncryptedSerShare {
        pub fn from_share(
            share: &DataShares,
            other_pk: &asym_ae::PublicKey,
            own_sk: &asym_ae::SecretKey,
        ) -> EncryptedSerShare {
            let encoded = serialize(share).expect("Unable to serialize a share in the constructor");
            let nonce = asym_ae::gen_nonce();
            let ciphertext = box_::seal(&encoded[..], &nonce, other_pk, own_sk);

            EncryptedSerShare {
                u: share.user_index,
                v: share.peer_index,
                nonce: nonce.0.iter().map(|x| *x).collect::<Vec<u8>>(),
                ciphertext,
            }
        }

        ///Decrypts and deserializes the Encrypted Share
        pub fn to_data_share(
            &self,
            other_pk: &asym_ae::PublicKey,
            own_sk: &asym_ae::SecretKey,
        ) -> DataShares {
            let nonce = asym_ae::Nonce::from_slice(&self.nonce[..]).unwrap();
            let plaintext = asym_ae::open(&self.ciphertext, &nonce, other_pk, own_sk)
                .expect("Unable to decrypt the data share");
            deserialize(&plaintext).expect("Unable to deserialize the share.")
        }
    }

    #[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
    pub struct EncryptedShareVec {
        pub message: Vec<EncryptedSerShare>,
    }
}

pub mod unmasking {
    use super::*;

    #[derive(Serialize, Deserialize, PartialEq)]
    pub struct UnmaskingShareVec {
        pub shares: Vec<UnmaskingShare>,
    }

    #[derive(Serialize, Deserialize, PartialEq)]
    pub struct UnmaskingShare {
        pub owner_index: usize,

        pub peer_index: usize,

        pub is_key_share: bool,

        pub share: SerializableShare,
    }

}
