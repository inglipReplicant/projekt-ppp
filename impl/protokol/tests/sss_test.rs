extern crate openssl;
extern crate protokol;

use openssl::bn::BigNum;
use protokol::sss::*;

#[test]
fn test_secret_gen() {
    let secret = BigNum::from_dec_str("1337").unwrap(); //our secret
    let prime = BigNum::from_dec_str("131071").unwrap(); //6th Mersenne prime
    let sss = ShamirScheme {
        prime: prime,
        num_shares: 6,
        threshold: 3,
    };
    let shares = sss.gen_shares(&secret).unwrap();
    assert_eq!(shares.len(), 6);
}

#[test]
fn test_reconstruction() {
    let prime = BigNum::from_u32(1613).unwrap();
    let sss = ShamirScheme {
        prime: prime,
        num_shares: 6,
        threshold: 3,
    };

    let x_1 = BigNum::from_u32(1).unwrap();
    let y_1 = BigNum::from_u32(1494).unwrap();
    let s_1 = Share { x: x_1, y: y_1 };

    let x_2 = BigNum::from_u32(2).unwrap();
    let y_2 = BigNum::from_u32(329).unwrap();
    let s_2 = Share { x: x_2, y: y_2 };

    let x_3 = BigNum::from_u32(3).unwrap();
    let y_3 = BigNum::from_u32(965).unwrap();
    let s_3 = Share { x: x_3, y: y_3 };

    let x_4 = BigNum::from_u32(4).unwrap();
    let y_4 = BigNum::from_u32(176).unwrap();
    let s_4 = Share { x: x_4, y: y_4 };

    let x_5 = BigNum::from_u32(5).unwrap();
    let y_5 = BigNum::from_u32(1188).unwrap();
    let s_5 = Share { x: x_5, y: y_5 };

    let secret = BigNum::from_u32(1234).unwrap();
    let mut shares = Vec::new();
    shares.push(s_1);
    shares.push(s_2);
    shares.push(s_3);
    shares.push(s_4);
    shares.push(s_5);

    let reconstructed = sss.recover_secret(&shares).unwrap();
    assert_eq!(reconstructed, secret);

    shares.remove(0);
    shares.remove(1);
    let reconstructed = sss.recover_secret(&shares).unwrap();
    assert_eq!(reconstructed, secret);
}

#[test]
fn test_gen_and_recon() {
    let secret =
        BigNum::from_hex_str("9f9f45e12715851eaa052e3f74a28dc3dc17aebe7955494060437e189a3a2514")
            .unwrap();
    let sss = ShamirScheme::from_bn(&secret, 10, 5).unwrap();
    let shares = sss.gen_shares(&secret).unwrap();
    let recovered = sss.recover_secret(&shares).unwrap();

    assert_eq!(sss.num_shares, shares.len());
    assert_eq!(recovered, secret);
}
