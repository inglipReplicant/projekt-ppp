extern crate protokol;
extern crate sodiumoxide;

use std::{iter, thread, time::Duration};

use std::thread::sleep;

use protokol::client::Client;
use protokol::shared::NUM_OF_USRS;
use sodiumoxide::crypto::box_;

fn main() {
    sodiumoxide::init().unwrap();
    let items: Vec<_> = iter::repeat(0).take(NUM_OF_USRS).collect();

    let threads: Vec<_> = items
        .into_iter()
        .map(|_| {
            thread::spawn(|| {
                sleep(Duration::from_millis(1_000));
                let (pubkey, seckey) = box_::gen_keypair();
                let mut client = Client::from_ip(&"127.0.0.1:8000", pubkey, seckey).unwrap();
                client.init_secure_channel().unwrap();
                if client.is_empty_nonce() || client.is_empty_key() {
                    println!("There are enough peers already. Aborting");
                    client.terminate_stream().unwrap();
                }

                client.run_round().unwrap();
            })
        })
        .collect();
    for handle in threads {
        handle.join().unwrap();
    }
    println!("Round done!");
}
