@startuml
participant Server as S
participant Klijent as K

loop za svakog drugog korisnika

    K -> K: <latex> s_{u, v} \leftarrow \text{dh_agree}(s_u^{SK}, s_v^{PK})</latex>
    K -> K: <latex> \textbf{p}_{u, v} \leftarrow \pm \text{prg_seeded}(s_{u, v}) </latex>
    note left: predznak je pozitivan ako u > v, \na negativan ako u < v
    K -> K: <latex> \textbf{p}_u \leftarrow \text{prg_seeded}(b_u) </latex>
    K -> K: <latex> \textbf{y}_u \leftarrow (\textbf{x}_u + \textbf{p}_u + \sum_v{\textbf{p}_{u, v}}) \text{mod} R</latex>
    K -> S: <latex> \text{ae_send}(\textbf{y}_u) </latex>
    ...kada je server prikupio barem t poruka i vrijeme je isteklo...
    loop za svakog klijenta u
        S -> K: <latex> \text{ae_send}([v]) </latex>
        note right: [v] - indeksi svih klijenata koji \nsu poslali maskirane vrijednosti
    end

@enduml
