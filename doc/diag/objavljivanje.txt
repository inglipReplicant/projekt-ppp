@startuml
participant Server
participant Klijent

Klijent -> Klijent: <latex> (s_u^{PK}, s_u^{SK}) \leftarrow \text{gen_keypair}() </latex>
Klijent -> Klijent: <latex> (c_u^{PK}, c_u^{SK}) \leftarrow \text{gen_keypair()} </latex>
Klijent -> Server: <latex> \text{ae_send}(s_u^{PK} || c_u^{PK}) </latex>

alt Server je prikupio barem t poruka
    |||
    loop za sve ostale korisnike
        Server -> Klijent: <latex> \text{ae_send}(v || c_v^{PK} || s_v^{PK}) </latex>
    end
    |||
else Vrijeme je isteklo i broj poruka je ispod t 
    |||
    Server -> Klijent: <latex> \text{kraj}() </latex>

end
@enduml
